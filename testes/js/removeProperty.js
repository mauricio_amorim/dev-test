// Implemente a função removeProperty, que recebe um objeto e o nome de uma propriedade.

// Faça o seguinte:

// Se o objeto obj tiver uma propriedade prop, a função removerá a propriedade do objeto e retornará true;
// em todos os outros casos, retorna falso.

function removeProperty(obj, prop) {
  if(typeof(obj) === 'object' && obj.hasOwnProperty(prop)){
    return delete(obj.prop);
  };
  return false;
}

console.log( removeProperty({t1:1,t2:2}, "t2"));